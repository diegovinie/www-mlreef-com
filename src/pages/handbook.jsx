import React from 'react';
import { graphql, Link } from "gatsby";
import Layout from '../layout';
import Header from '../components/Header';
import Footer from '../components/Footer/Footer';

const HandBookPage = props => {
  const postEdges = props.data.allMarkdownRemark.edges;

  console.log(postEdges);

  const previewCards = postEdges.map((ed, index) => (
    <div key={ ed.node.id } className="col-12 col-sm-6 p-3">
      <Link to={ `handbook/${ed.node.id}` } style={{ textDecoration: "none" }}>
        { ed.node.excerpt }
      </Link>
    </div>
  ));

  return (
    <Layout>
      <Header />
      <main className="container home-page">
        <h1 className="text-center py-4">Handbook static page example</h1>
        <div className="row px-2 py-3">
          { previewCards }
        </div>
      </main>
      <Footer config={ {
        copyright: '© 2020 by MLreef GmbH.',

      } } />
    </Layout>
  );
};

export const query = graphql`
  query HandBookPageQuery {
    allMarkdownRemark(
      filter: {
        frontmatter: {
          type: { eq: "handbook" }
        }
      }
    ) {
      edges {
        node {
          fields {
            slug
            date
          }
          id
          excerpt
          timeToRead
        }
      }
    }
  }
`;

export default HandBookPage;
