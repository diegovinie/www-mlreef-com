import React from 'react';
import './index.scss';
import Layout from '../layout';
import Header from '../components/Header';
import Footer from '../components/Footer/Footer';
import HomeStatement from '../components/HomeStatement';
import HomeFeatures from '../components/HomeFeatures';
import HomeContact from '../components/HomeContact';
import InputAction from '../components/forms/InputAction';

const image1APath = '/images/landing/Experiment_Details_A.webp';
const image1BPath = '/images/landing/Experiments_Overview_B.webp';
const image2APath = '/images/landing/Experiments_Pipeline_B.webp';
const image2BPath = '/images/landing/Experiments_Overview_B.webp';
const image3APath = '/images/landing/Data_Rep_Overview_C.webp';
const image3BPath = '/images/landing/Experiments_Pipeline_B.webp';


const HomePage = props => (
  <Layout>
    <Header />
    <main className="home-page">
      <section className="home-page-section bg-dark text-white">
        <HomeStatement
          title="The ideal nurturing ground for best Machine Learning."
          text="MLReef is a collaborative platform for version control on data and models to ensure end-to-end replicability and time efficiency in all your ML projects."
        />
      <div className="mb-4">
        <div className="col-12 text-center my-4">
          <h4>Do you want to get notified when we launch MLReef?</h4>
            <h4>Enter your email below:</h4>
        </div>
        <div className="col-10 col-sm-4 mx-auto">
          <InputAction
            placeholder="Enter your email here*"
            buttonLabel="Join"/>
        </div>
        </div>

      </section>

      <HomeFeatures features={[
        {
          title: 'End-to-end Pipeline',
          text: 'Fully customize your end-to-end ML pipeline from public or private data sets to your final experiments.',
          image: '/images/landing/endend-01.webp'
        },
        {
          title: 'Version Control',
          text: 'Enjoy full version control on your data and your models to ensure 100% replicability of your results.',
          image: '/images/landing/fork-01.webp'
        },
        {
          title: 'Community Content',
          text: 'Use community generated data sets, data operations and algorithms to get started in no time!',
          image: '/images/landing/content-01.webp'
        }
      ]}/>
      <HomeStatement
        title="ML made efficient and replicable"
        text="MLReef integrates the complete ML pipeline, from data management and pre-processing, data visualization and model management."
        mainImage={image1APath}
        optImage={image1BPath}
        />

      <HomeStatement
        title="Keep your style and flexibility"
        text="MLReef brings structure to chaos without limiting your flexibility and your working style. Collaborate with the whole MLreef community and your team to create the best ML outcome."
        mainImage={image2APath}
        optImage={image2BPath}
        />
      <HomeStatement
        title="Manage your data set like a pro"
        text={`MLReef works as a "single source of truth" for your data sets and it is powered by distributed version control (GIT). Improve your data set quality the same way you do with source code.`}
        mainImage={image3APath}
        optImage={image3BPath}
        />
      <HomeFeatures features={[
        {
          title: 'for everyone',
          text: 'MLReef supports open source for free. Get involved to perfect your craft and be part of something big.',
          image: '/images/landing/everyone-01.webp'
        },
        {
          title: 'for individuals',
          text: 'Use MLReef for your own ML projects, from experimental to hosting your life´s work.',
          image: '/images/landing/individual-01.webp'
        },
        {
          title: 'for teams',
          text: 'Business of all sizes use MLReef to securely and efficiently develope in teams their ML projects.',
          image: '/images/landing/team-01.webp'
        }
      ]} />
    <section className="bg-light pt-1">
      <HomeStatement
        title="Share your ML experience!"
        text="By providing your experience, you help us build the best experience for MLreef." />
      <div className="text-center col-12 mb-5 pb-5">
        <button className="btn btn-success py-2 px-4 font-weight-bold">
          Start the survey!
        </button>
      </div>
      <HomeContact />
    </section>
    </main>
    <Footer config={ {
      copyright: '© 2020 by MLreef GmbH.',

    } } />
  </Layout>
);

export default HomePage;
