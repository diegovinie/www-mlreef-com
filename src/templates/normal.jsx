import React from 'react';
import { graphql } from 'gatsby';
import Layout from '../layout';
import Header from '../components/Header';
import Footer from '../components/Footer/Footer';

const Normal = props => {
  const { html } = props.data.markdownRemark

  return (
    <Layout>
      <Header />
      <main className="container handbook-page">
        <div className="row px-2 py-3">
          <div className="handbook-page_content" dangerouslySetInnerHTML={{ __html: html }}></div>
        </div>
      </main>
      <Footer config={ {
        copyright: '© 2020 by MLreef GmbH.'
      } } />
    </Layout>
  );
};

export const query = graphql`
  query HandbookSectionById($id: String) {
    markdownRemark(id: {eq: $id}) {
      html
    }
  }
`;

export default Normal;
