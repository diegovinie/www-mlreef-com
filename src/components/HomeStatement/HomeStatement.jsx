import React from 'react';
import './HomeStatement.scss';

/**
 * @param {String} props.title required.
 * @param {String} props.text text under title.
 * @param {String} props.mainImage image's url.
 * @param {String} props.title image's url (requires props.mainImage).
 */
const HomeStatement = props => (
  <div className="home-statement mx-auto px-3 mb-5 text-center">
    <h3 className="home-statement-title mb-4">{ props.title }</h3>
    { props.text && <p className="home-statement-text mt-3 mb-4">{ props.text }</p> }
    { props.mainImage &&
      <div className="home-statement-images row mx-auto">
        <img className="main-image w-100 col-12 col-sm-8 px-0" src={props.mainImage} />
        { props.optImage && <img className="opt-image d-none d-sm-block border-light" src={props.optImage} />}
      </div>}
  </div>
);

export default HomeStatement;
