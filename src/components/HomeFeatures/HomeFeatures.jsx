import React from 'react';
import './HomeFeatures.scss';

/**
* @param {Array[Object]} props.features required.
* @param {String} props.features[{title}] required.
* @param {String} props.features[{text}] required.
* @param {String} props.features[{image}] image's url.
 */
const HomeFeatures = props => (
  <section className="home-features">
    {props.features.map(fea => (
      <div key={fea.title} className="home-features-item text-primary">
        { fea.image && 
          <div className="home-features-item-image my-4 mx-auto">
            <img src={fea.image} alt={fea.title} />
          </div>
        }
        <h5 className="home-features-item-title my-4 text-center">{ fea.title }</h5>
        <p className="home-features-item-text my-4 text-center">{ fea.text }</p>
      </div>
    ))}
  </section>
);

export default HomeFeatures;