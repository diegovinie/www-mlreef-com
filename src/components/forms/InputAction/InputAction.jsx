import React from 'react';
import './InputAction.scss';

const InputAction = props => (
  <div className="input-action input-group">
    <input
        className="form-control"
        placeholder={ props.placeholder }
        type={ props.type || 'text' } />
      <div className="input-action-append input-group-append">
        <button className="btn btn-info px-sm-4">
          { props.buttonLabel || 'send' }
        </button>
      </div>
  </div>
);

export default InputAction;
