import React from 'react'
import {Link} from 'gatsby';
import './Header.scss';

const logoPath = '/images/MLReef_Logo_POS_H-01.png';

const Header = props => (
  <header className="header bg-light py-1 py-sm-3">
    <div className="header-section left">
      <Link to="/handbook">
        Handbook
      </Link>
    </div>
    <div className="header-section center">
      <Link to="/">
        <img className="header-brand m-auto" src={logoPath} />
      </Link>
    </div>
    <div className="header-section right">
    </div>
  </header>
);

export default Header;
