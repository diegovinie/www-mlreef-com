Introduction
====================
The MLReef team handbook is the central repository for how we run the company. As part of our value of being transparent
the handbook is open to the world, and we welcome feedback. Please make a merge request to suggest improvements or add clarifications.
Please use issues to ask questions.


CEO
--------------------
This section details processes specific to the vision and mission of the company. The page is intended to be helpful,
feel free to deviate from it and update this page if you think it makes sense.

### Core Values
The company MLReef is based on the following three core values. These values inform everything that we do. 


#### Standard Operating Procedures
* none yet



Engineering
--------------------
This section details the development processes of MLReef.

#### Standard Operating Procedures
* [create gitlab project](engineering/sops/create-gitlab-project.md)

### Communication Culture
Engineering values clear and concise, low-context, asynchronous, non-intrusive, and frequent communication. Our communication
is done in English, so it is always possible to share conversations with other team members.  

What does that mean?

* **Clear and concise:** We aim to be clear in our communication. Each communication artifact (eg. an Email, comment, ticket, etc.)
  should require as little context as possible, already including descriptions, clear references like ticket numbers and links,
  and, if not completely obvious, some context to the regards of the message.
  
  This also means actively keeping topics on one channel, as far as possible and **NOT** scattering the communication
  between Gitlab comments, slack messages and emails - which would make it impossible to follow the flow of the conversation
  or share it with other team members.   

* **Low-context:** There exists a great [Wikipedia article](https://en.wikipedia.org/wiki/High-context_and_low-context_cultures#Examples_of_higher_and_lower_context_cultures)
  about the difference of high context and low context cultures, which contains concepts which are also applicable here.
  > Typically [in] a low-context culture […] individuals communicating will have fewer relational cues when interpreting messages.
  > Therefore, it is necessary for more explicit information to be included in the message […].
  > Not all individuals in a culture can be defined by cultural stereotypes […].
  > However, understanding the broad tendencies [… can help …] to better facilitate communication between individuals …
  
  **Typical Higher-context cultures:** African, Arabic, Persian, most Asian, most southern European, Slavic, Russian, Indians, Latin Americans
    
  **Lower-context cultures:** Almost all Anglo-Germanic, Israelis, Scandinavian

* **Asynchronous:** Especially engineers, need a lot of uninterrupted time to work. Realtime channels like Slack, Calls and Meetings,
interrupt this flow (see also the [makers schedule](working-model.md)). By actively choosing asynchronous, non-realtime
channels like email and Gitlab Comments, we allow the recipients to answer on their own time.

* **Non-intrusive:** Notifications are distracting to the receiver. Every time you send an avoidable notification to someone
you are wasting their time by disrupting their workflow and increase their anxiety and stress levels.
On the other hand, of course, interrupting is sometimes necessary, for urgent matters. This is why we aim to
minimize notifications to help our teammates. This has the added benefit of making the remaining notifications count more.

At MLReef we have a plethora of communication channels at our disposal. Email, Slack, Gitlab Comments and Mentions,
Google Meet, Skype, Whats App, Telephone, and more. Each of those channels brings their own communication culture with it.
Additionally, each culture, age group and even individual uses those channels differently.
Thus `number_of_channels x individuals x number_of_cultures = total_amount_of_different_expectations`

In this section we are trying to define a framework of communication behaviours which are globally accepted within MLReef.

#### MLReef communication framework
* Conversations regarding Tickets and Merge Requests are held within the ticket comments.
* Threads are used to separate different topics within Tickets, Merge Requests and Slack Channels

#### Response Times
* Slack: between minutes and hours
* Email: between Hours maximum two days
* Gitlab: between half days till the end of the week 



People Operations
--------------------

### Code of conduct
Our code of conduct can be found [here](code-of-conduct.md)

### Roles
* CEO
* [CTO](people-operations/chief-technology-officer.md)
* [Backend Developer](people-operations/backend-developer.md)
* [Frontend Developer](people-operations/frontend-developer.md)
* Machine Learning Developer


### Working Model
Working at MLReef is always governed by our [core values](Core Values).

* bias for action
* directly responsible individual
* handbook first approach
* minimal viable merge request
* respect the makers schedule

you can read a more detailed description on this [handbook page](working-model.md)


#### Standard Operating Procedures
Here you can find the most common People Operations Standard Operating Procedures

* [How to welcome new team members](sops/welcoming-sop.md)
