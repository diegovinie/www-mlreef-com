Chief Technology Officer at MLReef
====================


## Responsibilities
* Author project plans for milestones
* Generate and implement process improvements
* Make sure the handbook is used and maintained in a transparent way
* Spend time with customers to understand their needs and issues
* Own the sense of psychological safety of the department
* Hold regular 1:1's with all members of their team
* Give regular and clear feedback around the individual's performance
* Define and run the agile project management processes
* Provide mentoring for all engineers to help them grow in their technical responsibilities and remove increase their autonomy
* Conduct code reviews, and make technical contributions to product architecture as well as getting involved in solving bugs and delivering small features
* Craft code that meets our internal standards for style, maintainability, and best practices for a high-scale web environment. Maintain and advocate for these standards through code reviews and other measures
* Improve product quality, security, and performance
* Be accountable for product quality, security, and performance
* Foster technical decision making on the team, but make final decisions when necessary
* Actively seek and hire globally-distributed talent
* Conduct managerial interviews for candidates, and train the team to do technical interviews
* Own _thought leadership_ for maintaining the technical edge of the company
