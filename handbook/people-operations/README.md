People Operations
====================

Welcome to the People Group handbook! You should be able to find answers to most of your questions here. You can also check out pages related to People Operations in the next section below. If you can't find what you're looking for please do the following:


Role of People Operations
--------------------
In general, people operations is a service to the rest of the team; helping make your life easier so that you can focus on your work and contributions to MLReef. On that note, please don't hesitate to reach out with questions!

* [finding new team members](sops/qualification-sop.md)
* [welcoming new team members](sops/welcoming-sop.md)



Headquaters and Field Offices
--------------------

MLReef does neither have a headquater, nor official office locations.

Nevertheless there are [unofficial locations](locations.md)
