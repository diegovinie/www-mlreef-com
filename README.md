![Build Status](https://gitlab.com/mlreef/www-mlreef-com/badges/master/build.svg)

---
This repository contains the source code of the official MLReef homepage www.mlreef.com.

As part of our value of being transparent we welcome feedback. Please make a
[merge request](https://gitlab.com/mlreef/www-mlreef-com/merge_requests) to
suggest improvements or add clarifications. Please use
[issues](https://gitlab.com/mlreef/www-mlreef-com/issues) to ask questions.

---

Based on [Gatsby Advanced Starter](https://github.com/Vagr9K/gatsby-advanced-starter)
