---
type: handbook
---

MLReef Deep Dive 2020 Vienna
====================

Please know that the event is **not** mandatory.
Some of the benefits of attending the event are an opportunity to meet the other team members, and learn about the company, our product and the people that make the MLReef happen, so attendance is encouraged.
Significant Others are also encouraged to join, and contribute to the event! Keeping in mind that this is a _work trip_, so the priority is placed on work-related interactions.

### Questions?
After reading the info below, please check with @rainerkern for any questions you might have

**Note:** Please read through this README prior to reaching out through email.

### Who?
All current MLReef team members as of 12 Weeks before arrival day of the event. More recent members, depending on the budgetary situation.

### What?
Deep Dive is our MLReef team event. 2020 is going to be our first such event held at least annualy, but ideally more frequently. We get together to collaborate, build community, and get some work done! Since our team is scattered all over the globe, we try to plan a different location for each MLReef Deep Dive.

### When?
_Arrival day_ is 8th of March and departures day is 15th of March. Based on your location, you may need to depart your home on 7th of March to arrive on the 8th.

The _booking deadline_ is generally 6 Weeks before the arrival day.

### Where?
We'll be staying in Vienna. Further details will be released here.

### How?
As a team member, you can coordinate with @rainerkern for booking travel tickets.

### What is covered?
MLReef pays for all costs related to the event for team members.
This includes the following:

* Round trip travel
* All meals and drinks at the venues we are having our program, e.g. the hotel, an offsite venue, our party location
* Visa costs related to the event location (including visiting the Embassy)



General Info
--------------------
### The Agenda
_The agenda will be updated as it takes shape_



A few things to remember:
--------------------
- Booking travel
  If you want to book the travel yourself, please follow the following travel budgets. All amounts are listed in Euros (EUR). Convert the amount to your local currency if you don't use EUR.
  - 200€ for team members in EMEA
  - 700€ for team members in LATAM, and APAC
- Travel budget is not applicable for Significant Other's travel expenses.



FAQs
--------------------
### What if I can't find a flight within the budget for my region?
Please keep an eye on prices until you're about 2 weeks away from your booking deadline. You can set up an alert to be notified in price changes on Google Flights. Please do searches on Google Flights, Checkfelix, airline websites, and other travel sites to see whether there's a price difference.

Sometimes, traveling is cheaper when departing when arriving earlier or departing later - even including lodging costs. Please also check if this would be a viable solution in your case.

If you are still unable to find something that works, please reach out to @rainerkern, and we will do our best to accommodate you. Please note that you must reach out before booking your flight and gain approval for booking outside of your budget. Failure to do so may result in covering the overage out of pocket.

### When should I arrive/depart?
Plan to arrive on the arrival day (this means you may need to leave your home earlier). This is arrivals day and there are no planned activities for this day. Even if you arrive at 23:45 you will not miss out on anything and the team will be there to check you in.
If you arrive early on the arrival day, you may not be able to check into your hotel room right away, but the team will be there to greet you, and the hotel can hold your luggage so you're free to explore the city.
On departures day; you can leave at any time on that day as the event concludes the night before and no activities are planned for this day.

### What if I want to extend my trip and travel on different days?
If you want to extend your trip, it's fine to book and expense your travel on different days, provided it costs no more than traveling directly to/from Vienna.

### I need a visa for the Event. What happens if my visa application is declined and I need to cancel my travel plans?
First of all, we hope this doesn't happen to you! But if it did, any cancellation fees would be taken care of by MLReef.

This is also applicable if you are unable to apply for visa before the deadline for booking flights. You can book your tickets before the deadline if you are unable to apply for a visa at that time. That way, you get better rates on your tickets. We can cancel the tickets based on your status of Visa later, and
MLReef will take care of any cancellation fees.

[Requirements for travel documents](https://europa.eu/youreurope/citizens/travel/entry-exit/non-eu-nationals/index_en.htm)

There is a [list of countries](https://eur-lex.europa.eu/legal-content/en/TXT/?uri=CELEX:32018R1806) whose nationals do not need a visa to visit the EU for three months or less. The list of countries whose nationals require visas to travel to the United Kingdom or Ireland differs slightly from other EU countries.

> List of third countries whose nationals are exempt from the requriement to be in posession of a visa when crossing
> the external borders of the member states for stays of no more than 90 days in a 180-day period.

At the time of writing this list included the following countries.
Please check official pages like the [website of the austrian embassy in the USA](https://www.austria.org/do-you-need-a-visa)

* former Yugoslav Republic of Macedonia (special rules apply)
* Andorra
* United Arab Emirates (special rules apply)
* Antigua and Barbuda
* Albania (special rules apply)
* Argentina
* Australia
* Bosnia and Herzegovina (special rules apply)
* Barbados
* Brunei
* Brazil
* Bahamas
* Canada
* Chile
* Colombia
* Costa Rica
* Dominica (special rules apply)
* Micronesia (special rules apply)
* Grenada (special rules apply)
* Georgia (special rules apply)
* Guatemala
* Honduras
* Israel
* Japan
* Kiribati (special rules apply)
* Saint Kitts and Nevis
* South Korea
* Saint Lucia (special rules apply)
* Monaco
* Moldova (special rules apply)
* Montenegro (special rules apply)
* Marshall Islands (special rules apply)
* Mauritius
* Mexico
* Malaysia
* Nicaragua
* Nauru (special rules apply)
* New Zealand
* Panama
* Peru (special rules apply)
* Palau (special rules apply)
* Paraguay
* Serbia (special rules apply)
* Solomon Islands
* Seychelles
* Singapore
* San Marino
* El Salvador
* Timor-Leste (special rules apply)
* Tonga (special rules apply)
* Trinidad and Tobago
* Tuvalu (special rules apply)
* Ukraine (special rules apply)
* United States
* Uruguay
* Holy See
* Saint Vincent and the Grenadines (special rules apply)
* Venezuela
* Vanuatu (special rules apply)
* Samoa
