---
type: handbook
---

MLReef Coding Conventions
===================
The audience for our code is not computers, but rather our fellow developers.
This makes it our responsibility to write code which is easily understood by humans.



Kotlin
-------------------

We follow the official [Kotlin Coding Conventions](https://kotlinlang.org/docs/reference/coding-conventions.html)
with the following additions and exceptions …


### Continuation Indent Exception

We use `4` spaces instead of Continuation Indent instead of IntelliJ's default `8`.


### File Naming Addition
Kotlin files containing only a single non-private class, interface, or function
must have the same name as the class, interface, or function.

This naming is also the preferred naming for files containing only one main class
with helper classes which should be private but cannot be made private for framework reasons.


`MyFooClass.kt`
```Kotlin
class MyFooClass {…}

priate fun support() {…}

private class HelperClass()
```

Kotlin files containing multiple public classes, interfaces, or functions shall
have a descriptive name. Using _Util_ is discurraged for reasons laid out below.

Kotlin files containing multiple declarations are to be named in `lower-kebab-case`.


`file-containing-multiple-declarations.kt`
```Kotlin
class MyFooClass {…}

priate fun support() {…}

private class HelperClass()
```


--------------------

Testing
--------------------
Testing of APIs (gitlab, redis, etc.) has to be performed against our own infrastructure or a dedicated test system. **NOT** against the production services (e.g. gitlab.com)


Is Util evil?
--------------------

Yes! and No!
“Util” as a name does not contain any information. It is semantically the same as “random", “stuff", or at best "helpers".
It does neither help your fellow developer nor yourself to find a specific functionality.
Thus naming a class or a file just “Utils” is the same as not giving it any name at all - just a file containing random stuff.
Thing about "ConfigUtils" versus "ConfigAdapter" or "ConfigParser".

But "util" is even problematic on another level. It also does discourage the next contributor from good organisation.
Because there is already the messy util-file which already contains all kinds of functionality, another person is tempted to add their code in the same way - and so making the problem worse.

Because we want and need clear, expressive and self-documenting code, “util” as a name is highly discouraged and should only be used if it is the only useful naming option. Usually, you can be more specific.

